import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;

/**
 * Created by owlowl on 27.07.17.
 */
public class AppCounterTest {
    @Test
    public void getPathToSave() throws Exception {
        final String relativePath="src/main/resources/воскресение.txt";
        final File raw = new File(relativePath);
        final Path toSave = AppCounter.getPathToSave(raw.toPath());
        Assert.assertEquals("src/main/resources/воскресение_cleaned.txt",toSave.toString());
    }

}