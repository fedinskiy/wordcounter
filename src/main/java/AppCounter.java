import com.google.common.base.Splitter;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created by owlowl on 25.07.17.
 */
public class AppCounter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppCounter.class);
    private static final String WORD_SEPARATOR = "[^\\p{IsAlphabetic}]+";
    private static final FlatMapFunction<String, String> SPLIT =
            str -> Splitter
                    .onPattern(WORD_SEPARATOR)
                    .omitEmptyStrings()
                    .split(str)
                    .iterator();

    public static void main(String[] args) {
        final File raw = new File("src/main/resources/воскресение.txt");

        AppCounter app = new AppCounter();
        File txt;
        try {
            txt = convertToUTF(raw);
        } catch (IOException e) {
           LOGGER.error("Ошибка конвертации: ",e);
           return;
        }
        String longestWord = app.getLongestWord(txt);
        LOGGER.debug("longest word= {}",longestWord);
    }

    private static File convertToUTF(File raw) throws IOException {
        final Path rawPath= raw.getAbsoluteFile().toPath();
        final Path savePath=getPathToSave(rawPath);

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(raw.getAbsoluteFile()), Charset.forName("CP1251")));
        final BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(savePath.toFile()),Charset.forName("UTF8")));

        String line;
        while (Objects.nonNull(line = reader.readLine())){
            writer.write(line);
            writer.newLine();
        }
        return savePath.toFile();
    }

    public static Path getPathToSave(Path rawPath) {
        final String EXT_SEP = ".";
        StringBuilder fileName=new StringBuilder(rawPath.getFileName().toString());
        fileName.insert(fileName.lastIndexOf(EXT_SEP),"_cleaned");
        return rawPath.resolveSibling(fileName.toString());
    }

    public String getLongestWord(File file){
        try (SparkSession spark = SparkSession.builder().appName("Simple Application").master("local[*]").getOrCreate()) {
            final Dataset<String> strings = spark.read().textFile(file.getAbsolutePath());

            String reduce = strings
                    .flatMap(SPLIT, Encoders.STRING())
                    .map(String::toLowerCase, Encoders.STRING())
                    .dropDuplicates()
                    .reduce((str1, str2) -> str1.length() >= str2.length() ? str1 : str2);

           return reduce;

        }
    }

}
