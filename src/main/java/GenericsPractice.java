import java.util.ArrayList;
import java.util.List;

/**
 * Created by owlowl on 10.10.17.
 */
public class GenericsPractice {
    public static void main(String[] args) {
        Dimension
        Parent p1 = new Parent(10);
        Parent p2 = new Parent(20);
        Child c1 = new Child(30);
        Child c2 = new Child(40);
        List<Parent> parentList = new ArrayList<Child>();
        List<? super Leggable & extends Parent> childList = new ArrayList<>();
        parentList.add(p1);
        parentList.add(p2);
        parentList.add(c1);
        parentList.add(c2);
//        childList=parentList;
        childList.add(c1);
        childList.add(c2);
        for (Object part: childList) {
            System.out.println(part);
        }
//        childList.stream().map(Child::getValue).forEach(System.out::println);

    }
}

interface Leggable {
    String leg();
}

class Parent {
    final int value;

    public Parent(int val) {
        value = val;
    }

    public int getValue() {
        return value;
    }
}

class Child extends Parent implements Leggable {
    public Child(int val) {
        super(val);
    }

    @Override
    public int getValue() {
        return super.getValue() + 1;
    }

    @Override
    public String leg() {
        return String.valueOf(getValue()+"legged");
    }

    @Override
    public String toString() {
        return "Child{"+getValue()+"}";
    }
}
